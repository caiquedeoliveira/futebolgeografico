import {Link} from 'react-router-dom'
import classes from './Error404.module.css'
import logo from '../../Assets/Img/logo.png'

export default function Error404(){
    return(
        <>
            <main className={classes.main}>
                <h2 className={classes.erro}>Erro 404</h2>
                <img src={logo} alt="Logo do Futebol Geográfico" />
                <p>Ops! Parece que o endereço que você digitou está incorreto...</p>
                <Link to="/">VOLTAR</Link>
            </main>
        </>
    )
}
