import classes from './Footer.module.css'
import instagram from '../../Assets/Img/instagram.png'
import twitter from '../../Assets/Img/twitter.png'

export default function Footer(){
    return(
        <footer className={classes.footer}>
            <div className={classes.contact}>
                <ul>
                    <li>
                        <a href="https://instagram.com/futebolgeografico" target="_blank">
                            <img src={instagram} alt="Instagram do Futebol Geográfico"/>
                        </a>  
                    </li>

                    <li>
                        <a href="https://twitter.com/futgeografico" target="_blank">
                            <img src={twitter} alt="Twitter do Futebol Geográfico"/>
                        </a> 
                    </li>
                </ul>
            </div>

            <div className={classes.caption}>
                <p>Desenvolvido por Caíque Rodrigues</p>
            </div>

        </footer>
    )
}