import classes from './Header.module.css'
import logo from '../../Assets/Img/logo.png'
import {Link} from 'react-router-dom'

export default function Header(){
    return(
        <>
        <header className={classes.header}>
            <nav className={classes.navbarHeader}>
                <div className={classes.logoHeader}>
                    <ul>
                        <li>
                            <Link to="/">
                            <img src={logo} alt="Logo do Futebol Geográfico"/>
                            </Link>
                        </li>
                    </ul>
                    <h1>Futebol Geográfico</h1>  
                </div>

                <ul className={classes.navbarList}>
                    <li className={classes.animationHeader}>
                        <Link to="explorar">Explorar</Link>
                    </li>

                    <li className={classes.animationHeader}>
                        <Link to="sobre">Sobre</Link>
                    </li>
                </ul>
            </nav>
        </header>
        </>
    )
}