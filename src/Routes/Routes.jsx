import { BrowserRouter, Routes, Route} from "react-router-dom";
import Header from "../Components/Header/Header";
import Footer from "../Components/Footer/Footer";

import Error404 from "../Pages/Error404/Error404";

export function AppRoutes(){
    return(
        <BrowserRouter>
            <Header/>
            <Routes>
                <Route path="/"></Route>
                <Route path={"*"} element={<Error404/>}/>
            </Routes>
            <Footer/>
        </BrowserRouter>
    )
}